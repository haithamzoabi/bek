<script>
var vSrc = LOCALS.domainName+'includes/jquery.validate.pack.js'; 
addScript(vSrc);
$.getScript(vSrc, function(){
    $("#contact-form").validate({
		rules: {
			txtname: { 
				required: true,   
				minlength: 5, 
			},
			txtemail: {
				required: true,
				email: true
			},
			txtphone: {
				required: true,
				maxlength: 11,
				minlength: 9,
				number: true
			},
			txtcontent: {
				required: true,
				minlength: 3
			}
		},
		messages: { 
			txtname: {
				  required:LOCALS.l_fullNameRequired,
				  minlength:LOCALS.l_fullNameRequired
				  },
			txtemail: LOCALS.l_validEmail,
			txtphone: {
				  number:LOCALS.l_numbersOnly,
				  required: LOCALS.l_phoneRequired,
				  minlength: LOCALS.l_phoneMinLenght,
				  maxlength: LOCALS.l_phoneMaxLenght
				  },
			txtcontent: {
				  required: LOCALS.l_contentRequired,
				  minlength: LOCALS.l_contentLenght
				  }
		},
		submitHandler: function(form) {
		   $.ajax({
				type:'POST',
				url: "sendmail.php",
				data: $(form).serialize(),
				success: function(a,b,c){	
					$('#contact-form').slideUp();
					$('#sent').show();
				}
			});
			return false;
		}

	});  
})
</script>
<div class="def-page">

	<div class="def-page-title">
		<span><?=$l_contactUs?></span>
	</div>

	<div class="def-page-content">
	
	
		<div class="textContent">
		<?
		    $q= "select c_text from simple_content where c_id='1' ";
		    $res= query($q);
		    $row = $res->fetch_row();
		    echo $row[0];
		?>
		</div>
	
		<div class="contactUsForm">
		<div id="sent">
			<?=$l_messageSent?>
		</div>
		<form id="contact-form" method="post">
            <div>
				<span class="label"><?=$l_name?></span>
            	<input class="input required error" type="text" name="txtname" title="What shall I call you?"/>
            </div>
            <div dir="ltr">
				<span class="label"><?=$l_phone?></span>	
            	<input class="input required error" type="text" name="txtphone" title="" dir="ltr"/>
            </div>
            <div dir="ltr">
				<span class="label"><?=$l_email?></span>	
            	<input class="input required error" type="text" name="txtemail" title="foo@bar.com" dir="ltr"/>
            </div>
            <div data-textarea>
				<span class="label"><?=$l_content?></span>	
            	<textarea class="txt-input required error" name="txtcontent" ></textarea>
            </div>
            <div>
            	<input class="submit" type="submit" name="submit" value="<?=$l_send?>"/>
            </div>
        </form>
		
		</div>
	
	</div>


</div>