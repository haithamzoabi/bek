<script>
	$(function(){
		$('table .tr_caption td,table tfoot td').attr('colspan',$('table .tr_title td').length);		
	});
</script>

<div class="pagediv">
	
	
	<table id="ntable">
		
		<colgroup>
			<col class="side1 column-index" />
			<col span="1" class="side3 column-actions" />
			<col span="3" class="side2 column-details"/>
		</colgroup>
		
		<thead>
			<tr class="tr_caption">
				<td><span><?= $l_adsList ?></span></td>
			</tr>
			
			<tr class="tr_title">
				<td data-for="column-index">&nbsp;</td>				
				<td data-for="column-action" width="20"><?= $l_update ?></td>
				<td ><?= $l_adTitle ?></td>
				<td> <?= $l_startDate ?></td>
				<td> <?= $l_endDate ?></td>
			</tr>
			
		</thead>
		
		<tbody>
			<?	

			$query1 = "select * from ads order by a_id desc";

			$queryres = query($query1);
			$adjacents = 5;
			$targetpage = "?menu=$menu&pageLimit=$pageLimit";
			$total_pages = $queryres->num_rows;
			$limit =(empty($pageLimit))?20:$pageLimit;
			//first item to display on this pg, if no pg var is given, set start to 0
			$pg = $_GET['pg'];			
			$start = ($pg) ? ($pg - 1) * $limit : 0;
			$mone=0;
			include('scripts/paging.php');
			$q = $query1." limit $start,$limit " ;
			$res = query ($q);

			$i = 0;
			while ($row = $res->fetch_row() ) {
				$i++;
			?>
			<tr>
				<td><?=$i?></td>
				<td align="center">
					<a title="<?= $l_update ?>" href="<?=$controlDomainName?>/ad_update?menu=<?=$_GET['menu']?>&sid=<?=$row[0]?>">
					<img border="0" src="<?=$controlDomainName?>images/edit.png" width="13" height="13" alt="">
					</a>
				</td>	
				<td><?= $row[1]?></td>
				<td><?= $row[7]?></td>
				<td><?= $row[8]?></td>
			</tr>
			<?
			}
			?>			
		</tbody>
			
		
		<tfoot>
			<tr>
				<td align="right" >
					<div class="totalDiv"><?="$l_totoal : $total_pages"?></div>
					<div class="pagination"><?=$pagination?></div>
				</td>
			</tr>
		</tfoot>
		
	</table>
	
	
</div>