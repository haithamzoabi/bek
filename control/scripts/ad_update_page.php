<script>
var form = $('#adsForm');
var fields = [{
	name: 'title',
	id:'title',
	type: 'text',
	label: LOCALS.l_adTitle
},{
	name:'location',
	id:'location',
	type: 'selection',
	label: LOCALS.l_adLocation,
	options: [{
		text: LOCALS.l_mainPage,
		value:0
	},{
		text: LOCALS.l_innerPage,
		value:1
	}]
},{
	name:'file',
	id:'file',
	type:'browser',
	label: LOCALS.l_file,
	onclick: 'openFileManager("flash",false ,"file")'
},{
	name: 'link',
	id: 'link',
	type: 'text',
	dir:'ltr',
	label: LOCALS.l_link
},{
	name:'linkTarget',
	id:'linkTarget',
	type:'selection',
	label: LOCALS.l_linkTarget,
	options: [{
		text: LOCALS.l_newPage,
		value:0
	},{
		text: LOCALS.l_samePage,
		value:1
	}]
},{
	name: 'startdate',
	id:'startdate',
	type:'date',
	label: LOCALS.l_startDate
},{
	name: 'enddate',
	id:'enddate',
	type:'date',
	label: LOCALS.l_endDate
},{
	name: 'details',
	id: 'details',
	type:'textarea',
	label: LOCALS.l_details
}];
var frm = generateForm('adsForm', LOCALS.l_updateAd , fields , ['update','delete'],'adsList');
$(function(){
	$('.pagediv').append(frm);
	createEditor('ar' , 'details' , editorReady );
	FUNC.sendFormData('adsForm','update');
});

function editorReady(editor){
	var formId = 'adsForm';
	FUNC.fetchData(formId, function(data){
		var linkTarget = data.linkTarget;
		var adLocation = data.location;
		$('#location').val(linkTarget);
	});
}

</script>

<div class="pagediv"> </div>
