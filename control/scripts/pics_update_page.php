<script>
	$(function() {

	
	var formId =  $('form').attr('id');
	FUNC.fetchData(formId , function(formData){
		FUNC.configSelection('picsCategories_list','lstCategories', formData.lstCategories );
		FUNC.configSelection('cities_list','lstCities' , formData.lstCities);
	});
	
	FUNC.sendFormData(formId,'update');
	
    });
</script>

<div class="pagediv">
    <?
    $formTitle = $l_updatesubject;
    $submitButtonText = $l_update;
    $sid = getGetInput('sid');
    include("pics_formPage.php");
    ?>

</div>    
