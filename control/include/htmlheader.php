<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<link rel="stylesheet" href="<?=$cssFilePath?>" type="text/css" media="screen">
	<script src="<?=$jqueryFilePath?>"></script>
	<script src="<?=$jsLOCALSfilePath?>"></script>
	<script src="<?=$jsFunctionsFilePath?>"></script>
	<script type="text/javascript" src="<?=$controlDomainName?>ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?=$controlDomainName?>ckeditor/lang/_languages.js"></script>
	<title><?=$l_websitePageTitle?></title>
</head>
<body>