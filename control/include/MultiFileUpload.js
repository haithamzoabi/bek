Ext.define('Ext.ux.form.MultiFile', {
    extend: 'Ext.form.field.File',
    alias: 'widget.multifilefield',
	createFileInput : function() {
        var me = this;
        me.fileInputEl = me.button.el.createChild({
            name: me.getName()+'[]',
            cls: Ext.baseCSSPrefix + 'form-file-input',
            tag: 'input',
            type: 'file',
			multiple:'true',
            size: 1
        }).on('change', me.onFileChange, me);
    },
 
    onFileChange: function (button, e, value) {
        this.duringFileSelect = true;
 
        var me = this,
            upload = me.fileInputEl.dom,
            files = upload.files,
            names = [];
		me.filesCount = files.length;
        if (files) {
            for (var i = 0; i < me.filesCount; i++)
                names.push(files[i].name);
            value = names.join(', ');
        }
 
        Ext.form.field.File.superclass.setValue.call(this, value);
 
        delete this.duringFileSelect;
    }
});
 