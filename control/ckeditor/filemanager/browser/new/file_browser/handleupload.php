<?
	/*
	$fileName = $_FILES['filedata']['name'];
	$tmpName  = $_FILES['filedata']['tmp_name'];
	$fileSize = $_FILES['filedata']['size'];
	$fileType = $_FILES['filedata']['type'];
	*/
	$sCommand = $_GET['Command'];
	$sResourceType	= $_GET['Type'];
	$sCurrentFolder = $_GET['CurrentFolder'].'/';
	
	require('./config.php') ;
	require('./util.php') ;
	require('./io.php') ;
	
	
	function reArrayFiles(&$file_post) {

		$file_ary = array();
		$file_count = count($file_post['name']);
		$file_keys = array_keys($file_post);

		for ($i=0; $i<$file_count; $i++) {
			foreach ($file_keys as $key) {
				$file_ary[$i][$key] = $file_post[$key][$i];
			}
		}

		return $file_ary;
	}
	
	
	if ( $sCommand == 'FileUpload' ){
		FileUpload( $sResourceType, $sCurrentFolder, $sCommand ) ;		
	}
	
	if ( $sCommand == 'MultiFileUpload' ){
		MultiFileUpload( $sResourceType, $sCurrentFolder, $sCommand ) ;		
	}

	if ( $sCommand == 'CreateFolder' ){
		CreateFolder( $sResourceType, $sCurrentFolder ) ;		
	}	
	
	if ( $sCommand == 'DeleteFile' ){
		DeleteFile( $_GET['fileName'] ,  $sCurrentFolder) ;		
	}	
	
	
	
	function MultiFileUpload ( $resourceType, $currentFolder, $sCommand , $CKEcallback = ''){
		if (!isset($_FILES)) {
			global $_FILES;
		}
		$filedUploaded= 0;
		$totalRenames = 0;
		$totalErrors = 0;
		$succeedFiles = array();
		$files_arr = reArrayFiles($_FILES['filedata']);
		foreach ($files_arr as $file) {
			$result = UploadSingleFile($file , $resourceType, $currentFolder, $sCommand);
			if ($result['sErrorNumber']=='0'){
				$filedUploaded++;
				array_push($succeedFiles , $result);
			}elseif ($result['sErrorNumber']=='201'){
				$filedUploaded++;
				$totalRenames++;
				array_push($succeedFiles , $result);
			}else{
				$totalErrors++;
			}
		}
		if($CKEcallback == ''){
			$success=($filedUploaded!=0)? 'true' : 'false';
			echo "{success:$success, filedUploaded:'$filedUploaded',totalRenames:'$totalRenames',totalErrors:'$totalErrors'}";
		}else{
			$customeMessage = ($sErrorNumber != 0  ? 'Error '. $sErrorNumber. ' upload failed. '. $sErrorMsg : 'Upload Successful');
			echo "{success:true, callback:'$CKEcallback' , files:'$succeedFiles', customMsg:'$customeMessage'}";
		}
		exit;
	}
	function UploadSingleFile ($file , $resourceType, $currentFolder, $sCommand ){
		$sErrorNumber = '0';
		$sFileName= '';
		if ( isset($file)  &&  !is_null($file['tmp_name']) ){
			global $Config;
			$oFile = $file;
			
			$sServerDir = ServerMapFolder( $resourceType, $currentFolder, $sCommand ) ;
			
			$sFileName = $oFile['name'] ;
			$sFileName = SanitizeFileName( $sFileName ) ;
			$sOriginalFileName = $sFileName ;
			
			$sExtension = substr( $sFileName, ( strrpos($sFileName, '.') + 1 ) ) ;
			$sExtension = strtolower( $sExtension ) ;
			
			if ( IsAllowedExt( $sExtension, $resourceType ) ){
				$iCounter = 0 ;
				while (true){
					$sFilePath = $currentFolder . $sFileName ;
					if ( is_file( $sFilePath ) ){ // file exist
						$iCounter++ ;
						$sFileName = RemoveExtension( $sOriginalFileName ) . '(' . $iCounter . ').' . $sExtension ;
						$sErrorNumber = '201' ;
					}else{ // file does not exist
						move_uploaded_file( $oFile['tmp_name'], $sFilePath ) ;
						if ( is_file($sFilePath) ){ // if added give permissions 
							if ( isset( $Config['ChmodOnUpload'] ) && !$Config['ChmodOnUpload'] ){
								break ;
							}
							$permissions = 0777;	 
							if ( isset( $Config['ChmodOnUpload'] ) && $Config['ChmodOnUpload'] ){
								$permissions = $Config['ChmodOnUpload'] ;
							}
							$oldumask = umask(0) ;
							chmod( $sFilePath, $permissions ) ;
							umask( $oldumask );							
						}
						break;
					}
				}
			}else{
				$sErrorNumber = '202';
			}
			
		}else{
			$sErrorNumber = '203';
		}
		
		$sFileUrl = $currentFolder ;
		$sFileUrl = CombinePaths( $sFileUrl, $sFileName );
		return array ( 'sFileUrl'=>$sFileUrl ,  'sFileName'=>$sFileName , 'sErrorNumber'=>$sErrorNumber );
	
	}
	
	
	
	
	// Notice the last paramter added to pass the CKEditor callback function
	function FileUpload( $resourceType, $currentFolder, $sCommand, $CKEcallback = '' ){
		if (!isset($_FILES)) {
			global $_FILES;
		}
		
		$sErrorNumber = '0' ;
		$sFileName = '' ;
	 
			//PATCH to detect a quick file upload.
		if (( isset( $_FILES['filedata'] ) && !is_null( $_FILES['filedata']['tmp_name'] ) ) || (isset( $_FILES['upload'] ) && !is_null( $_FILES['upload']['tmp_name'] ) ))
		{
			global $Config ;
	 
					 //PATCH to detect a quick file upload.
			$oFile = isset($_FILES['filedata']) ? $_FILES['filedata'] : $_FILES['upload'];
	 
			// Map the virtual path to the local server path.
			$sServerDir = ServerMapFolder( $resourceType, $currentFolder, $sCommand ) ;
	 
			// Get the uploaded file name.
			$sFileName = $oFile['name'] ;
			$sFileName = SanitizeFileName( $sFileName ) ;
	 
			$sOriginalFileName = $sFileName ;
	 
			// Get the extension.
			$sExtension = substr( $sFileName, ( strrpos($sFileName, '.') + 1 ) ) ;
			$sExtension = strtolower( $sExtension ) ;
	 
			if ( isset( $Config['SecureImageUploads'] ) )
			{
				if ( ( $isImageValid = IsImageValid( $oFile['tmp_name'], $sExtension ) ) === false )
				{
					$sErrorNumber = '202' ;
				}
			}
	 
			if ( isset( $Config['HtmlExtensions'] ) )
			{
				if ( !IsHtmlExtension( $sExtension, $Config['HtmlExtensions'] ) &&
					( $detectHtml = DetectHtml( $oFile['tmp_name'] ) ) === true )
				{
					$sErrorNumber = '202' ;
				}
			}
	 
			// Check if it is an allowed extension.
			if ( !$sErrorNumber && IsAllowedExt( $sExtension, $resourceType ) )
			{
				$iCounter = 0 ;
	 
				while ( true )
				{
					//$sFilePath = $sServerDir . $sFileName ;					
					$sFilePath = $currentFolder . $sFileName ;
					
					if ( is_file( $sFilePath ) ){
						$iCounter++ ;
						$sFileName = RemoveExtension( $sOriginalFileName ) . '(' . $iCounter . ').' . $sExtension ;
						$sErrorNumber = '201' ;
					} else {
						move_uploaded_file( $oFile['tmp_name'], $sFilePath ) ;
	 
						if ( is_file( $sFilePath ) )
						{
							if ( isset( $Config['ChmodOnUpload'] ) && !$Config['ChmodOnUpload'] )
							{
								break ;
							}
	 
							$permissions = 0777;
	 
							if ( isset( $Config['ChmodOnUpload'] ) && $Config['ChmodOnUpload'] )
							{
								$permissions = $Config['ChmodOnUpload'] ;
							}
	 
							$oldumask = umask(0) ;
							chmod( $sFilePath, $permissions ) ;
							umask( $oldumask ) ;
						}
	 
						break ;
					}
				}
	 
				if ( file_exists( $sFilePath ) )
				{
					//previous checks failed, try once again
					if ( isset( $isImageValid ) && $isImageValid === -1 && IsImageValid( $sFilePath, $sExtension ) === false )
					{
						@unlink( $sFilePath ) ;
						$sErrorNumber = '202' ;
					}
					else if ( isset( $detectHtml ) && $detectHtml === -1 && DetectHtml( $sFilePath ) === true )
					{
						@unlink( $sFilePath ) ;
						$sErrorNumber = '202' ;
					}
				}
			}
			else
				$sErrorNumber = '202' ;
		}
		else
			$sErrorNumber = '202' ;
	 
		$sFileUrl = $currentFolder ;
		$sFileUrl = CombinePaths( $sFileUrl, $sFileName ) ;
	 
		if($CKEcallback == ''){
			SendUploadResults( $sErrorNumber, $sFileUrl, $sFileName ) ;
		} else {
			//issue the CKEditor Callback
			//SendCKEditorResults ($sErrorNumber, $CKEcallback, $sFileUrl, $sFileName);
			$customeMessage = ($sErrorNumber != 0  ? 'Error '. $sErrorNumber. ' upload failed. '. $sErrorMsg : 'Upload Successful');
			SendCKEditorResults ($CKEcallback, $sFileUrl, customeMessage);
		}
		exit ;
	}
	
	function SendUploadResults ($sErrorNumber, $sFileUrl, $sFileName ){
		$success=($sErrorNumber=='0')? 'true' : 'false';
		echo "{success:$success, file:'$sFileName' , sFileUrl:'$sFileUrl', sErrorNumber:'$sErrorNumber'}";
	}
	
	function SendCKEditorResults ($callback, $sFileUrl, $customMsg = ''){
		$success=($customMsg=='')?true : false;
		echo "{success:true, callback:'$callback' , sFileUrl:'$sFileUrl', customMsg:'$customMsg'}";

	}
	
	
	
	
function CreateFolder( $resourceType, $currentFolder )
{
	if (!isset($_GET)) {
		global $_GET;
	}	
	if (!isset($_POST)) {
		global $_POST;
	}
	
	$sErrorNumber	= '0' ;
	$sErrorMsg		= '' ;
	
	if ( isset( $_POST['NewFolderName'] ) )
	{
		$sNewFolderName = $_POST['NewFolderName'] ;
		$sNewFolderName = SanitizeFolderName( $sNewFolderName ) ;

		if ( strpos( $sNewFolderName, '..' ) !== FALSE )
			$sErrorNumber = '102' ;		// Invalid folder name.
		else
		{
			// Map the virtual path to the local server path of the current folder.
			//$sServerDir = ServerMapFolder( $resourceType, $currentFolder, 'CreateFolder' ) ;
			$sServerDir = $currentFolder;
			if ( is_writable( $sServerDir ) )
			{
				$sServerDir .= $sNewFolderName ;

				$sErrorMsg = CreateServerFolder( $sServerDir ) ;

				switch ( $sErrorMsg )
				{
					case '' :
						$sErrorNumber = '0' ;
						break ;
					case 'Invalid argument' :
					case 'No such file or directory' :
						$sErrorNumber = '102' ;		// Path too long.
						break ;
					default :
						$sErrorNumber = '110' ;
						break ;
				}
			}
			else
				$sErrorNumber = '103' ;
		}
	}
	else
		$sErrorNumber = '102' ;

	// Create the "Error" node.
	//echo '<Error number="' . $sErrorNumber . '" />' ;
	$success=($sErrorNumber=='0')? 'true' : 'false';
	echo "{success:$success, folder:'$sNewFolderName' , parent:'$currentFolder', sErrorNumber:'$sErrorNumber' , sServerDir:'$sServerDir'}";
}	


function DeleteFile($fileName, $currentFolder){
	$error= '';
	$path= $currentFolder.'/'.$fileName;
	if ( is_file($path) ){
		unlink($path);
	}else{
		$error ='file does not exist';
	}
	$success=($error=='')? 'true' : 'false';
	echo "{success:$success,path:'$path' ,error:'$error'}";
	exit;
}
function DeleteFolder(){

}
	
	
	
?>